// Writing JavaScript comments

// This is a single line comment

/*
	Multiline comment
	Sets comments for multiple lines of codes (ctrl + shift + /)
*/
console.log("Hello World");

// Variables 
// used to contain data

// Declaring variable

/*
	
	Syntax:
	let/const varibleName;

	let is a keyword that is usually used in declaring a variable
	const stores constant values that will not change over time
*/
let myVariable;
console.log(myVariable); // since not initialised, gives undefined in console

//let my Number; - not a valid variable name

// Declaring a variable with initial value

/*
	Syntax:
		let/const variableName = value;
*/
let productName = 'desktop computer';
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;

// Reassigning variable values
productName = 'Laptop';
console.log(productName);



/*
interest = 4;
console.log(interest)
Gives error as we cannot reassign constant variable
*/

// Multiple variable declaration
let productCode = 'Dc017'; productBrand = 'Dell';
console.log(productCode, productBrand);

// Declaring a variable with a reserved keyword
/*const let = "Hello";
console.log(let);*/

// Data Types

// String
let country = 'India';
let province = "New Delhi";

// Contantenation 
let fullAddress = province + ' ' + country;
console.log(fullAddress);

let greeting = 'I live in '+ country;
console.log(greeting);

// The escape character (\)
let mailAddress = 'New Delhi\n\nIndia';
console.log(mailAddress);

let message = "John's employees went home early";
console.log(message);

let headCount = 26;
console.log(headCount);

// Decimal numbers / fractions
let grade = 98.7
console.log(grade);

/*let message = 'John\'s \"employees\" went home early';
console.log(message)*/

// Boolean
let isMarried = true;
let inGoodConduct = true;
console.log("isMarried: "+ isMarried);
console.log("inGoodConduct: "+ inGoodConduct);

// Arrays
let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

let details = ["John", "Smith", 32, true];
console.log(details);

// Objects
/*
	Syntax:
	let/const objectName = {
		propertyA: value,
		propertyB: value
	}
*/

let person = {
	fullName: 'Onkar Yewale',
	age: 29,
	isMarried: true,
	contact: ["+91909 999 5758", "+91727 682 4147"],
	address: {
		houseNumber: '2A',
		city: 'Navi' + ' ' + 'Mumbai'
	}
}

console.log(person);

let myGrades = {
	firstGrading: 100,
	secondGrading: 100,
	thirdGrading: 90.2,
	fourthGrading: 94.6
}

console.log(myGrades);

// type of operator
console.log(typeof myGrades);

console.log(typeof grades);

// Null and undefined

// Null - expresses the absence of a value in a variable
let myNumber = 0;
let myString = '';

// Undefined - represents the state of a variable that has been declared but without value.
let fullName;

console.log(myNumber, myString, fullName);
